﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pg_class
{
    public partial class manager
    {
        /// <summary>
        /// Метод очищает лист подписок на события менеджера данных
        /// </summary>
        private void InvocationListClear()
        {
            //Завершение сесии ведения журнала
            //Вызов события журнала
            JournalEventArgs me = new JournalEventArgs(eEntity.manager, 0, "Сессия журнала закрыта", eAction.DisConnect, eJournalMessageType.information);
            JournalMessageOnReceived(me);
        }
    }
}
