﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;
using pg_class.pg_exceptions;
using System.Data;
using System.Net.Sockets;
using pg_class.pg_commands;

namespace pg_class.poolcn
{
    internal partial class connect
    {
        /// <summary>
        /// Сопоставление композитных типов
        /// </summary>
        protected void npgsql_type_map(NpgsqlConnection cn)
        {
            if (cn.State == ConnectionState.Open)
            {
                cn.TypeMapper.Reset();
                cn.TypeMapper.MapComposite<pg_argument>("argument");
            }
        }
    }
}
