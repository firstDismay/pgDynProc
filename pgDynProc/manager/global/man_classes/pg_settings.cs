﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pg_class
{
    /// <summary>
    /// Класс инициализации настроек библиотеки доступа к БД Учет
    /// </summary>
    public class pg_settings
    {
        #region КОНСТРУКТОРЫ КЛАССА
        /// <summary>
        /// Закрытый конструктор класса
        /// </summary>
        protected pg_settings()
        {
        }
        /// <summary>
        /// Основной конструктор инициализации класса настроек
        /// </summary>
        public pg_settings(String UserName, String Password, String Host, String DataBase, Int32 Port = 5999,
                String ApplicationName = "-",
                String SearchPath = "plg, public", Int32 SessionTimeOut = 30, Int32 Timeout = 120, Int32 CommandTimeout = 1800, Int32 PoolConnectMax = 10)
        {
            StringBuilder sb =  new StringBuilder();
            //Если имя вызывающего приложения не передано, передаем имя класса доступа к данным
            if (ApplicationName == "-")
            {
                sb.Append(System.Reflection.Assembly.GetExecutingAssembly().GetName().Name);
                sb.Append(", Ver=");
                sb.Append(System.Reflection.Assembly.GetExecutingAssembly().GetName().Version);
                ApplicationName_ = sb.ToString();
            }
            else
            {
                sb.Append(ApplicationName);
                sb.Append(", ");
                sb.Append(System.Reflection.Assembly.GetExecutingAssembly().GetName().Name);
                sb.Append(", Ver=");
                sb.Append(System.Reflection.Assembly.GetExecutingAssembly().GetName().Version);
                ApplicationName_ = sb.ToString();
            }
            //sur-asdb88.ogk.energo.local      uchet.asuscomm.com
            UserName_ = UserName;
            Host_ = Host;
            DataBase_ = DataBase;
            Port_ = Port;
            SearchPath_ = SearchPath;
            SessionTimeOut_ = SessionTimeOut;
            Timeout_ = Timeout;
            CommandTimeout_ = CommandTimeout;
            PoolConnectMax_ = PoolConnectMax;
            //ConnectionIdleLifetime_ = ConnectTimeOut * 60;


            pg_sb = new Npgsql.NpgsqlConnectionStringBuilder();

            //Настройки параметров SSL начало
            pg_sb.SslMode = Npgsql.SslMode.Require;
            pg_sb.TrustServerCertificate = true;
            //Настройки параметров SSL конец

            pg_sb.ApplicationName = ApplicationName_;
            pg_sb.Host = Host_;
            pg_sb.Port = Port_;
            pg_sb.Database = DataBase_;
            pg_sb.SearchPath = SearchPath_;
            pg_sb.Username = UserName_;
            pg_sb.Password = Password;
            pg_sb.CommandTimeout = CommandTimeout_;
            pg_sb.InternalCommandTimeout = -1;
            pg_sb.Timeout = Timeout_;
            //pg_sb.TcpKeepAlive = false;
            //pg_sb.KeepAlive = 30;
            pg_sb.Pooling = false;
            //pg_sb.ConnectionIdleLifetime = ConnectionIdleLifetime_;
            pg_sb.ClientEncoding = "UTF8";
            pg_sb.Encoding = "UTF8";
        }

        #endregion

        #region СВОЙСТВА КЛАССА

        private Npgsql.NpgsqlConnectionStringBuilder pg_sb;
        private String ApplicationName_;

        /// <summary>
        /// Наименование приложение в формате предоставляемом в строке подключения
        /// </summary>
        public String ApplicationName
        {
            get
            {
                return ApplicationName_;
            }
        }
        /// <summary>
        /// Строка подключенияя
        /// </summary>
        public string NpgsqlConnectionString
        {
            get
            {
                if (pg_sb != null)
                {
                    return pg_sb.ToString();
                }
                else
                {
                    return null;
                }
            }       
        }

        String SearchPath_;
        /// <summary>
        /// Путь поиска по умолчанию
        /// </summary>
        public string SearchPath
        {
            get
            {
                return SearchPath_;
            }
            set
            {
                SearchPath_ = value;
                pg_sb.SearchPath = SearchPath_;
            }
        }

        String Host_;
        /// <summary>
        /// Сетевое имя или IP адрес сервера БД
        /// </summary>
        public string Host
        {
            get
            {
                return Host_;
            }
            set
            {
                Host_ = value;
                pg_sb.Host = Host_;
            }
        }

        Int32 Port_;
        /// <summary>
        /// Порт ожидания запросов
        /// </summary>
        public int Port
        {
            get
            {
                return Port_;
            }
            set
            {
                Port_ = value;
                pg_sb.Port = Port_;
            }
        }

        Int32 PoolConnectMax_;
        /// <summary>
        /// Максимальное количество подключений в сесии менеджера
        /// </summary>
        public int PoolConnectMax
        {
            get
            {
                return PoolConnectMax_;
            }
            set
            {
                PoolConnectMax_ = value;
            }
        }
        String DataBase_;
        /// <summary>
        /// Название БД
        /// </summary>
        public string DataBase
        {
            get
            {
                return DataBase_;
            }
            set
            {
                DataBase_ = value;
                pg_sb.Database = DataBase_;
            }
        }

                private String UserName_;
        /// <summary>
        /// Роль пользователя БД
        /// </summary>
        public String UserName
        {
            get
            {
                return UserName_;
            }
            set
            {
                UserName_ = value;
                pg_sb.Username = UserName_;
            }
        }

       /// <summary>
       /// Пароль роли БД если применим по условиям авторизации
       /// </summary>
       public String Password
       {
           set
           {
                pg_sb.Password = value;
           }
       }
        /// <summary>
        /// Возвращает или задает логическое значение, которое определяет, возвращаются ли сведения, связанные с 
        /// безопасностью (такие как пароль), как часть подключения, если оно открыто или когда-либо находилось в открытом состоянии.
        /// </summary>
        public Boolean PersistSecurityInfo
        {
            get
            {
                return pg_sb.PersistSecurityInfo;
            }
            set
            {
                pg_sb.PersistSecurityInfo = value;
            }
        }
       private Double DataTimeOut_;
       /// <summary>
       /// Время удержания неактивного соединения в минутах
       /// </summary>
       public Double DataTimeOut
        {
            get
            {
                return DataTimeOut_;
            }
            set
            {
                DataTimeOut_ = value;
            }
       }
        

        private Int32 CommandTimeout_;
        /// <summary>
        /// Время ожидания (в секундах) при попытке выполнить команду, прежде чем завершить попытку и выдать ошибку. Установить на ноль для бесконечности.
        /// </summary>
        public Int32 CommandTimeout
        {
            get
            {
                return CommandTimeout_;
            }
            set
            {
                CommandTimeout_ = value;
                pg_sb.CommandTimeout = CommandTimeout_;
            }
        }

       
        private Int32 SessionTimeOut_;
        /// <summary>
        /// Время бездействия клиентского приложения в минутах, до вызова метода закрытия соединения с сервером
        /// </summary>
        public Int32 SessionTimeOut
        {
            get
            {
                return SessionTimeOut_;
            }
            set
            {
                SessionTimeOut_ = value;
            }
        }

        private Int32 Timeout_;
        /// <summary>
        /// Время ожидания при попытке установить соединение в секундах
        /// </summary>
        public Int32 Timeout
        {
            get
            {
                return Timeout_;
            }
            set
            {
                Timeout_ = value;
                pg_sb.Timeout = Timeout_;


            }
        }

        private String UploadTemp_;
            /// <summary>
            /// Наименование временной директории для загрузки файлов библиотеки документов
            /// </summary>
        public String UploadTemp
            {
                get {
                        if (UploadTemp_ == null)
                        {
                            UploadTemp_ = "$uchet$";
                        }
                        return UploadTemp_;
                    }
                set { UploadTemp_ = value; }
            }

        


        #endregion
    }
}
