﻿using System;
using System.Collections.Generic;
using NpgsqlTypes;

namespace pg_class
{
    partial class manager
    {
        #region ФУНКЦИИ СОПОСТАВЛЕНИЯ ТИПОВ ДАННЫХ Postgre SQL и .NET
        /// <summary>
        /// Функция определяет тип данных net столбца таблицы
        /// </summary>
        private Type Name_To_Type(String NameType, String CategoryType = "S")
        {
            Type ResType = null;
            if (CategoryType == "A")

            {
                ResType = typeof(System.Array);
            }
            else
            {
                switch (NameType)
                {
                    case "int2":
                        ResType = typeof(Int16);
                        break;
                    case "int4":
                        ResType = typeof(Int32);
                        break;
                    case "int8":
                        ResType = typeof(Int64);
                        break;
                    case "varchar":
                        ResType = typeof(String);
                        break;
                    case "name":
                        ResType = typeof(String);
                        break;
                    case "uuid":
                        ResType = typeof(String);
                        break;
                    case "numeric":
                        ResType = typeof(Decimal);
                        break;
                    case "bool":
                        ResType = typeof(Boolean);
                        break;
                    case "text":
                        ResType = typeof(String);
                        break;
                    case "oid":
                        ResType = typeof(UInt32);
                        break;
                    case "bytea":
                        ResType = typeof(Byte[]);
                        break;
                    case "json":
                        ResType = typeof(String);
                        break;
                    case "money":
                        ResType = typeof(Decimal);
                        break;
                    case "time":
                        ResType = typeof(TimeSpan);
                        break;
                    case "float4":
                        ResType = typeof(Single);
                        break;
                    case "float8":
                        ResType = typeof(Double);
                        break;
                    case "date":
                        ResType = typeof(DateTime);
                        break;
                    case "timestamp":
                        ResType = typeof(DateTime);
                        break;
                    case "interval":
                        ResType = typeof(TimeSpan);
                        break;
                    case "argument":
                        ResType = typeof(pg_argument);
                        break;
                    case "tsrange":
                        ResType = typeof(NpgsqlRange<DateTime>);
                        break;
                    default:
                        ResType = typeof(String);
                        break;
                }
            }
            return ResType;
        }

        /// <summary>
        /// Функция определяет тип данных параметра функции
        /// </summary>
        private NpgsqlTypes.NpgsqlDbType Name_To_NpgsqlType(String NameType)
        {
            NpgsqlTypes.NpgsqlDbType Result = NpgsqlTypes.NpgsqlDbType.Varchar;
            NpgsqlTypes.NpgsqlDbType TValue;
            if (Dictionary_Name_To_NpgsqlType.TryGetValue(NameType, out TValue))
                Result = TValue;
            return Result;
        }

        private static readonly Dictionary<String, NpgsqlTypes.NpgsqlDbType> Dictionary_Name_To_NpgsqlType = new Dictionary<String, NpgsqlTypes.NpgsqlDbType>()
        {
            { "int2", NpgsqlDbType.Smallint },
            { "int4", NpgsqlDbType.Integer },
            { "int8", NpgsqlDbType.Bigint },
            { "varchar", NpgsqlDbType.Varchar },
            { "name", NpgsqlDbType.Name },
            { "numeric", NpgsqlDbType.Numeric },
            { "bool", NpgsqlDbType.Boolean },
            { "text", NpgsqlDbType.Text },
            { "oid", NpgsqlDbType.Oid },
            { "bytea", NpgsqlDbType.Bytea },
            { "json", NpgsqlDbType.Json },
            { "array", NpgsqlDbType.Array },
            { "money", NpgsqlDbType.Money },
            { "time", NpgsqlDbType.Time },
            { "float4", NpgsqlDbType.Real },
            { "float8", NpgsqlDbType.Double },
            { "date", NpgsqlDbType.Date },
            { "timestamp", NpgsqlDbType.Timestamp },
            { "timestamptz", NpgsqlDbType.TimestampTz },
            { "interval", NpgsqlDbType.Interval },
            {"int4range" , NpgsqlDbType.Range | NpgsqlDbType.Integer},
            {"int8range" , NpgsqlDbType.Range | NpgsqlDbType.Bigint},
            {"numrange" , NpgsqlDbType.Range | NpgsqlDbType.Numeric},
            { "tsrange", NpgsqlDbType.Range | NpgsqlDbType.Timestamp},
            {"tstzrange" , NpgsqlDbType.Range | NpgsqlDbType.TimestampTz},
            {"daterange" , NpgsqlDbType.Range | NpgsqlDbType.Date},
            { "_int2", NpgsqlDbType.Array | NpgsqlDbType.Smallint },
            { "_int4", NpgsqlDbType.Array | NpgsqlDbType.Integer },
            { "_int8", NpgsqlDbType.Array | NpgsqlDbType.Bigint },
            { "_varchar", NpgsqlDbType.Array | NpgsqlDbType.Varchar },
            { "_name", NpgsqlDbType.Array | NpgsqlDbType.Name },
            { "_numeric", NpgsqlDbType.Array | NpgsqlDbType.Numeric },
            { "_bool", NpgsqlDbType.Array | NpgsqlDbType.Boolean },
            { "_text", NpgsqlDbType.Array | NpgsqlDbType.Text },
            { "_oid", NpgsqlDbType.Array | NpgsqlDbType.Oid },
            { "_money", NpgsqlDbType.Array | NpgsqlDbType.Money },
            { "_time", NpgsqlDbType.Array | NpgsqlDbType.Time },
            { "_float4", NpgsqlDbType.Array | NpgsqlDbType.Real },
            { "_float8", NpgsqlDbType.Array | NpgsqlDbType.Double },
            { "_date", NpgsqlDbType.Array | NpgsqlDbType.Date },
            { "_timestamp", NpgsqlDbType.Array | NpgsqlDbType.Timestamp },
            { "_timestamptz", NpgsqlDbType.Array | NpgsqlDbType.TimestampTz },
            { "_interval", NpgsqlDbType.Array | NpgsqlDbType.Interval }
        };

        //***********************************************
            /// <summary>
            /// Функция определяет тип данных свойства в среде Postgre SQL
            /// </summary>
        public NpgsqlTypes.NpgsqlDbType DataTypeNpgsql(eDataType Datatype)
        {
            NpgsqlTypes.NpgsqlDbType Result = NpgsqlTypes.NpgsqlDbType.Unknown;
            NpgsqlTypes.NpgsqlDbType TValue;
            if (Dictionary_DataTypeNpgsql.TryGetValue(Datatype, out TValue))
                Result = TValue;
            return Result;
        }
        private static readonly Dictionary<eDataType, NpgsqlTypes.NpgsqlDbType> Dictionary_DataTypeNpgsql = new Dictionary<eDataType, NpgsqlTypes.NpgsqlDbType>()
        {
            { eDataType.val_varchar, NpgsqlTypes.NpgsqlDbType.Varchar },
            { eDataType.val_int, NpgsqlTypes.NpgsqlDbType.Integer },
            { eDataType.val_numeric, NpgsqlTypes.NpgsqlDbType.Numeric },
            { eDataType.val_real, NpgsqlTypes.NpgsqlDbType.Real },
            { eDataType.val_double, NpgsqlTypes.NpgsqlDbType.Double },
            { eDataType.val_money, NpgsqlTypes.NpgsqlDbType.Money },
            { eDataType.val_text, NpgsqlTypes.NpgsqlDbType.Text },
            { eDataType.val_bytea, NpgsqlTypes.NpgsqlDbType.Bytea },
            { eDataType.val_boolean, NpgsqlTypes.NpgsqlDbType.Boolean },
            { eDataType.val_date, NpgsqlTypes.NpgsqlDbType.Date },
            { eDataType.val_time, NpgsqlTypes.NpgsqlDbType.Time },
            { eDataType.val_interval, NpgsqlTypes.NpgsqlDbType.Interval },
            { eDataType.val_timestamp, NpgsqlTypes.NpgsqlDbType.Timestamp },
            { eDataType.val_json, NpgsqlTypes.NpgsqlDbType.Json },
            { eDataType.val_bigint, NpgsqlTypes.NpgsqlDbType.Bigint }
        };

        ///////**************************
            /// <summary>
            /// Функция определяет тип данных свойства в среде .Net 
            /// </summary>
        public eDataTypeNet DataTypeNet(eDataType Datatype)
        {
            eDataTypeNet Result = eDataTypeNet.Object;
            eDataTypeNet TValue;
            if (Dictionary_DataTypeNet.TryGetValue(Datatype, out TValue))
                Result = TValue;
            return Result;
        }

        private static readonly Dictionary<eDataType, eDataTypeNet> Dictionary_DataTypeNet = new Dictionary<eDataType, eDataTypeNet>() 
        {
            { eDataType.val_varchar, eDataTypeNet.String },
            { eDataType.val_int, eDataTypeNet.Int32 },
            { eDataType.val_numeric, eDataTypeNet.Decimal },
            { eDataType.val_real, eDataTypeNet.Single },
            { eDataType.val_double, eDataTypeNet.Double },
            { eDataType.val_money, eDataTypeNet.Decimal },
            { eDataType.val_text, eDataTypeNet.String },
            { eDataType.val_bytea, eDataTypeNet.ByteArray },
            { eDataType.val_boolean, eDataTypeNet.Boolean },
            { eDataType.val_date, eDataTypeNet.DateTime },
            { eDataType.val_time, eDataTypeNet.TimeSpan },
            { eDataType.val_interval, eDataTypeNet.TimeSpan },
            { eDataType.val_timestamp, eDataTypeNet.DateTime },
            { eDataType.val_json, eDataTypeNet.String },
            { eDataType.val_bigint, eDataTypeNet.Int64 }
        };

        #endregion

        #region ФУНКЦИИ СОПОСТАВЛЕНИЯ СУЩНОСТЕЙ Postgre SQL и .NET
            
            /// <summary>
            /// Нименовании сущности по данным перечисления типа сущности
            /// </summary>
        public static String EntityName(eEntity EntityType)
        {
            String Result = "неизвестная сущность";
            String TValue;
            if (Dictionary_EntityName.TryGetValue(EntityType, out TValue))
                Result = TValue;
            return Result;
        }

        private static readonly Dictionary<eEntity, string> Dictionary_EntityName = new Dictionary<eEntity, String>()
        {
            
            { eEntity.entity, "Сущность базы данных" },
            { eEntity.manager, "Менеджер данных" },
            { eEntity.pool, "Пул соединений менеджера данных" },
            { eEntity.connect, "Соединение пула соединений" },
            { eEntity.function, "Функция сервера" }
        };

        /// <summary>
        /// Текстовое описание операции по данным перечисления типов операций
        /// </summary>
        public static String ActionDesc(eAction Action)
        {
            String Result = "Неизвестное действие";
            switch (Action)
            {
                case eAction.Copy:
                    Result = "Копирование";
                    break;
                case eAction.Delete:
                    Result = "Удаление";
                    break;
                case eAction.Insert:
                    Result = "Добавление";
                    break;
                case eAction.Insert_mass:
                    Result = "Добавление массовое";
                    break;
                case eAction.Lock:
                    Result = "Блокировка";
                    break;
                case eAction.Move:
                    Result = "Перемещение";
                    break;
                case eAction.Select:
                    Result = "Выборка";
                    break;
                case eAction.UnLock:
                    Result = "Разблокировка";
                    break;
                case eAction.Update:
                    Result = "Обновление";
                    break;
                case eAction.Execute:
                    Result = "Выполнение функции";
                    break;
                case eAction.Connect:
                    Result = "Подключен к базе";
                    break;
                case eAction.ReConnect:
                    Result = "Переподключение к базе";
                    break;
                case eAction.DisConnect:
                    Result = "Отключен от базы";
                    break;
                case eAction.Init:
                    Result = "Инициализация";
                    break;
                case eAction.Clone:
                    Result = "Клонирование";
                    break;
                case eAction.RollBack:
                    Result = "Отмена";
                    break;
                case eAction.Clear:
                    Result = "Очистка";
                    break;
                case eAction.Cast:
                    Result = "Приведение";
                    break;
            }
            return Result;
        }

        #endregion

        #region Обработка ошибок методов БД
        /// <summary>
        /// Функция определяющая базовый код ошибки сущности
        /// </summary>
        public static eEntity_ErrID Entity_To_ErrID(eEntity Entity)
        {
            eEntity_ErrID Result = eEntity_ErrID.manager;

            switch (Entity)
            {
                case eEntity.entity:
                    Result = eEntity_ErrID.entity;
                    break;
                case eEntity.manager:
                    Result = eEntity_ErrID.manager;
                    break;
                case eEntity.pool:
                    Result = eEntity_ErrID.pool;
                    break;
                case eEntity.connect:
                    Result = eEntity_ErrID.connect;
                    break;
                case eEntity.function:
                    Result = eEntity_ErrID.function;
                    break;
            }
            return Result;
        }

        /// <summary>
        /// 
        /// </summary>
        public static String SubClass_ErrDesc(eSubClass_ErrID SubClass_ErrID)
        {
            String Result = "Без описания";

            switch (SubClass_ErrID)
            {
                case eSubClass_ErrID.SCE0_Unknown_Error:
                    Result = "Неизвестная ошибка";
                    break;
                case eSubClass_ErrID.SCE1_NonExistent_Entity:
                    Result = "Несуществующая сущность";
                    break;
                case eSubClass_ErrID.SCE2_Violation_Unique:
                    Result = "Нарушение уникальности";
                    break;
                case eSubClass_ErrID.SCE3_Violation_Rules:
                    Result = "Нарушение правил или ограничений";
                    break;
                case eSubClass_ErrID.SCE4_Violation_Rules_Nesting:
                    Result = "Нарушение правил вложенности";
                    break;
                case eSubClass_ErrID.SCE5_Violation_Limitations_Nesting:
                    Result = "Нарушение ограничений вложенности";
                    break;
                case eSubClass_ErrID.SCE6_Over_Limitations_Nesting:
                    Result = "Превышение ограничения вложенности";
                    break;
                case eSubClass_ErrID.SCE7_NonExistent_Inherited_Entity:
                    Result = "Несуществующая наследуемая сущность";
                    break;
                case eSubClass_ErrID.SCE8_NonExistent_Parental_Entity:
                    Result = "Несуществующая родительская сущность";
                    break;
                case eSubClass_ErrID.SCE9_Out_Of_Range:
                    Result = "Выход за пределы допустимых значений";
                    break;
                case eSubClass_ErrID.SCE10_NonExistent_Source_Entity:
                    Result = "Несуществующая исходная сущность";
                    break;
            }
            return Result;
        }

        #endregion
    }
}
