﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace pg_class
{
    #region Перечисления общих параметров менеджера данных и методов доступа к данным и действий
       

    /// <summary>
    /// Перечисление определяющее тип действия выполняемого методом доступа к БД
    /// </summary>
    public enum eAction
    {
        /// <summary>
        /// Любое действие, значение по умолчанию
        /// </summary>
        AnyAction = 0,
        /// <summary>
        /// Операция выборки
        /// </summary>
        Select = 7000,
        /// <summary>
        /// Операция вставки
        /// </summary>
        Insert = 1000,
        /// <summary>
        /// Операция обновления
        /// </summary>
        Update = 2000,
        /// <summary>
        /// Операция удалеия
        /// </summary>
        Delete = 3000,
        /// <summary>
        /// Операция переноса, перемещения
        /// </summary>
        Move = 4000,
        /// <summary>
        /// Операция блокировки сущности
        /// </summary>
        Lock = 9000,
        /// <summary>
        /// Операция разблокировки сущности
        /// </summary>
        UnLock = 10000,
        /// <summary>
        /// Операция копирования, в том числе вложенных сущностей
        /// </summary>
        Copy = 5000,
        /// <summary>
        /// Операция подключения к БД
        /// </summary>
        Connect = 11000,
        /// <summary>
        /// Операция переподключения к БД
        /// </summary>
        ReConnect = 12000,
        /// <summary>
        /// Операция отключения от БД
        /// </summary>
        DisConnect = 13000,
        /// <summary>
        /// Выполнение функции
        /// </summary>
        Execute = 14000,
        /// <summary>
        /// Инициализация сущности БД
        /// </summary>
        Init = 15000,
        /// <summary>
        /// Операция клонирования сущности
        /// </summary>
        Clone  = 6000,
        /// <summary>
        /// Откат сущности к ранее сохраненному состоянию
        /// </summary>
        RollBack = 8000,
        /// <summary>
        /// Очистка сущности от неиспользуемых или поврежденных данных
        /// </summary>
        Clear = 16000,
        /// <summary>
        /// Приведение сущности к указанному виду
        /// </summary>
        Cast = 17000,
        /// <summary>
        /// Операция массовой вставки
        /// </summary>
        Insert_mass = 18000,
        /// <summary>
        /// Операция слияния
        /// </summary>    
        Merging = 18000
    }

    /// <summary>
    /// Типы сообщений журнала конфигуратора
    /// </summary>
    public enum eJournalMessageType
    {
        /// <summary>
        /// Онформационное сообщение
        /// </summary>
        information,
        /// <summary>
        /// Сообщение об ошибке
        /// </summary>
        error,
        /// <summary>
        /// Сообщение об успешном завершении
        /// </summary>
        success,
        /// <summary>
        /// Низкоуровневые сообщения
        /// </summary>
        debug
    }
      
     
    /// <summary>
    /// Состояния менеджера данных
    /// </summary>
    public enum eManagerState
    {
        /// <summary>
        /// Менеджер Не готов к работе
        /// </summary>
        NoReady,
        /// <summary>
        /// Менеджер подключен к базе и инициализирован
        /// </summary>
        Connected,
        /// <summary>
        /// Менеджер инициализирован и отключен от базы все соединения разорваны по таймауту учетные данные сессии определены
        /// </summary>
        Disconnected,
        /// <summary>
        /// Менеджер инициализирован, Пользователь вышел из всех сессий, все соединения разорваны учетные данные сброшены
        /// </summary>
        LogOff
    }

    #endregion

    #region Перечисления типов данных
    /// <summary>
    /// Перечисление допустимых типов данных свойств PostgreSQL
    /// </summary>
    public enum eDataType
    {
        /// <summary>
        /// Тип VarChar
        /// </summary>
        val_varchar = 1, //val_varchar
        /// <summary>
        /// Тип Integer
        /// </summary>
        val_int = 2, //val_int
        /// <summary>
        /// Тип Numeric
        /// </summary>
        val_numeric = 3, //val_numeric
        /// <summary>
        /// Тип Real
        /// </summary>
        val_real = 4, //val_real
        /// <summary>
        /// Тип Double
        /// </summary>
        val_double = 5, //val_double
        /// <summary>
        /// Тип Money
        /// </summary>
        val_money = 6, //val_money
        /// <summary>
        /// Тип Text
        /// </summary>
        val_text = 7, //val_text
        /// <summary>
        /// Тип Bytea
        /// </summary>
        val_bytea = 8, //val_bytea
        /// <summary>
        /// Тип Boolean
        /// </summary>
        val_boolean = 9, //val_boolean
        /// <summary>
        /// Тип Date
        /// </summary>
        val_date = 10, //val_date
        /// <summary>
        /// Тип Time
        /// </summary>
        val_time = 11, //val_time
        /// <summary>
        /// Тип Interval
        /// </summary>
        val_interval = 12, //val_interval
        /// <summary>
        /// Тип Time stamp
        /// </summary>
        val_timestamp = 13, //val_timestamp
        /// <summary>
        /// Тип Json
        /// </summary>
        val_json = 14, //val_json
        /// <summary>
        /// Тип BigInt
        /// </summary>
        val_bigint = 15 //val_bigint
    }

    /// <summary>
    /// Перечисление типов данных .Net
    /// </summary>
    public enum eDataTypeNet
    {
        /// <summary>
        /// Тип данных Boolean
        /// </summary>
        Boolean,
        /// <summary>
        /// Тип данных Byte
        /// </summary>
        Byte,
        /// <summary>
        /// Тип данных ByteArray
        /// </summary>
        ByteArray,
        /// <summary>
        /// Тип данных Char
        /// </summary>
        Char,
        /// <summary>
        /// Тип данных DateTime
        /// </summary>
        DateTime,
        /// <summary>
        /// Тип данных TimeSpan
        /// </summary>
        TimeSpan,
        /// <summary>
        /// Тип данных DBNull
        /// </summary>
        DBNull,
        /// <summary>
        /// Тип данных Decimal
        /// </summary>
        Decimal,
        /// <summary>
        /// Тип данных Double
        /// </summary>
        Double,
        /// <summary>
        /// Тип данных Empty
        /// </summary>
        Empty,
        /// <summary>
        /// Тип данных Int16
        /// </summary>
        Int16,
        /// <summary>
        /// Тип данных Int32
        /// </summary>
        Int32,
        /// <summary>
        /// Тип данных Int64
        /// </summary>
        Int64,
        /// <summary>
        /// Тип данных Object
        /// </summary>
        Object,
        /// <summary>
        /// Тип данных SByte
        /// </summary>
        SByte,
        /// <summary>
        /// Тип данных Single
        /// </summary>
        Single,
        /// <summary>
        /// Тип данных String
        /// </summary>
        String,
        /// <summary>
        /// Тип данных UInt16
        /// </summary>
        UInt16,
        /// <summary>
        /// Тип данных UInt32
        /// </summary>
        UInt32,
        /// <summary>
        /// Тип данных UInt64
        /// </summary>
        UInt64,
        /// <summary>
        /// Диапазан времени
        /// </summary>
        TSRange
    }

    #endregion

    #region Сущности
    /// <summary>
    /// Перечисление сущностей БД
    /// </summary>
    public enum eEntity
    {
        /// <summary>
        /// Обобщенная сущность
        /// </summary>
        entity = 0,
        
        /// <summary>
        /// Базовый класс доступа к данным, синглетон
        /// </summary>
        manager = 99,
        /// <summary>
        /// Пул соединений менеджера данных
        /// </summary>
        pool = 100,
        /// <summary>
        /// Соединение пула соединений
        /// </summary>
        connect = 101,
        /// <summary>
        /// Функция
        /// </summary>
        function = 102        
    }

    #endregion

    #region Перечисления классов и кодов ошибок менеджера данных БД

    /// <summary>
    /// Перечисление базовых кодов ошибок для сущностей БД
    /// </summary>
    public enum eEntity_ErrID
    {
        /// <summary>
        /// Обобщенная сущность
        /// </summary>
        entity = 10000000,
        /// <summary>
        /// Базовый класс доступа к данным, синглетон
        /// </summary>
        manager = 19900000,
        /// <summary>
        /// Пул соединений
        /// </summary>
        pool = 20000000,
        /// <summary>
        /// Соединение пула соединений
        /// </summary>
        connect = 20100000,
        /// <summary>
        /// Функция
        /// </summary>
        function = 20300000
    }

    /// <summary>
    /// Перечисление базовых кодов ошибок для методов БД
    /// </summary>
    public enum eSubClass_ErrID
    {
        /// <summary>
        /// Неизвестная ошибка - 0
        /// </summary>
        SCE0_Unknown_Error = 0,
        /// <summary>
        /// Несуществующий объект - 1
        /// </summary>
        SCE1_NonExistent_Entity = 1,
        /// <summary>
        /// Нарушение уникальности -2
        /// </summary>
        SCE2_Violation_Unique = 2,
        /// <summary>
        /// Нарушение правил или ограничений - 3
        /// </summary>
        SCE3_Violation_Rules = 3,
        /// <summary>
        /// Нарушение правил вложенности - 4
        /// </summary>
        SCE4_Violation_Rules_Nesting = 4,
        /// <summary>
        /// Нарушение ограничений вложенности - 5
        /// </summary>
        SCE5_Violation_Limitations_Nesting = 5,
        /// <summary>
        /// Превышение ограничения вложенности - 6
        /// </summary>
        SCE6_Over_Limitations_Nesting = 6,
        /// <summary>
        /// Несуществующий наследуемый объект - 7
        /// </summary>
        SCE7_NonExistent_Inherited_Entity = 7,
        /// <summary>
        /// Несуществующий родительский объект - 8
        /// </summary>
        SCE8_NonExistent_Parental_Entity = 8,
        /// <summary>
        /// Выход за пределы допустимых значений
        /// </summary>
        SCE9_Out_Of_Range = 9,
        /// <summary>
        /// Несуществующий исходный объект
        /// </summary>
        SCE10_NonExistent_Source_Entity = 10,
    }

    /// <summary>
    /// Перечисление источников ошибок с учетом версий функций определяемых типом аргумента результата
    /// </summary>
    public enum eSourceError
    {
        /// <summary>
        /// Функция класса с аргументом версии 1
        /// </summary>
        ClassFuncVer1,
        /// <summary>
        /// Функция класса с аргументом версии 2
        /// </summary>
        ClassFuncVer2,
        /// <summary>
        /// Функция сервера с аргументом версии 1
        /// </summary>
        ServerFuncVer1,
        /// <summary>
        /// Функция сервера с аргументом версии 2
        /// </summary>
        ServerFuncVer2
    }
    #endregion   
}
