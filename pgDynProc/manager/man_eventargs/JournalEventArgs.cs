﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace pg_class
{
    /// <summary>
    /// Аргумент сообщения журнала событий
    /// </summary>
    public class JournalEventArgs : EventArgs
    {
        #region КОНСТРУКТОРЫ КЛАССА

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        protected JournalEventArgs()
        {
            primaryerrordesc = "";
        }

        /// <summary>
        /// Основной конструктор класса аргумента события
        /// </summary>
        public JournalEventArgs(Int64 procoid, String procname, Int32 ErrorID, String ErrorDesc, eAction Action , eJournalMessageType MessageType) : this()
        {
            errorid = ErrorID;
            errordesc = ErrorDesc;
            action = Action;
            messagetype = MessageType;
            entityname = eEntity.function;
        }

        /// <summary>
        /// Основной конструктор класса аргумента события
        /// </summary>
        public JournalEventArgs(eEntity Entity, Int32 ErrorID, String ErrorDesc, eAction Action, eJournalMessageType MessageType) : this()
        {
            entityname = Entity;
            errorid = ErrorID;
            errordesc = ErrorDesc;
            action = Action;
            messagetype = MessageType;
        }

        /// <summary>
        /// Основной конструктор класса аргумента события
        /// </summary>
        public JournalEventArgs(eEntity Entity, Int32 ErrorID, String ErrorDesc, String PrimaryErrorDesc, eAction Action, eJournalMessageType MessageType) : this()
        {
            entityname = Entity;
            errorid = ErrorID;
            errordesc = ErrorDesc;
            primaryerrordesc = PrimaryErrorDesc;
            action = Action;
            messagetype = MessageType;
        }

        /// <summary>
        /// Дополнительный конструктор класса аргумента события
        /// </summary>
        public JournalEventArgs(JournalEventArgs JournalEventArgs) : this()
        {
            errorid = JournalEventArgs.ErrorID;
            errordesc = JournalEventArgs.ErrorDesc;
            action = JournalEventArgs.Action;
            messagetype = JournalEventArgs.MessageType;
        }

        #endregion

        #region СВОЙСТВА КЛАССА
        private Int64 procoid;
        private String procname;

        private eEntity entityname;
        private eAction action;
        private Int32 errorid;
        private String errordesc;
        private String primaryerrordesc;
        private eJournalMessageType messagetype;

        /// <summary>
        /// Перечисление определяющее тип действия выполняемого методом доступа к БД
        /// </summary>
        public eAction Action { get => action; }

        /// <summary>
        /// Тип сообщения журнала конфигуратора
        /// </summary>
        public eJournalMessageType MessageType
        {
        get
            {
                return messagetype;
            }
        }

        /// <summary>
        /// Идентификатор функции
        /// </summary>
        public Int64 ProcOID { get => procoid; }

        /// <summary>
        /// Наменовании сущности подвергшейся модификации
        /// </summary>
        public String EntityName
        {
            get
            {
                return manager.EntityName(entityname);
            }
        }
        /// <summary>
        /// Перечисление сущности подвергшейся модификации
        /// </summary>
        public eEntity eEntityName
        {
            get
            {
                return entityname;
            }
        }
        /// <summary>
        /// Системное наименование функции
        /// </summary>
        public String ProcName { get => procname; }

        /// <summary>
        /// Код завершения операции
        /// </summary>
        public Int32 ErrorID { get => errorid; }

        /// <summary>
        /// Текстовое описание результата операции
        /// </summary>
        public String ErrorDesc
        {
            get
            {
                StringBuilder Result = new StringBuilder();
                Result.Append(errordesc);
                if (!String.IsNullOrEmpty(primaryerrordesc))
                {
                    Result.Append(" Исходное сообщение: ");
                    Result.Append(primaryerrordesc);
                }
                return Result.ToString();
            }
        }

        /// <summary>
        /// Текстовое описание операции
        /// </summary>
        public String ActionDesc
        {
            get
            {
                return manager.ActionDesc(action);
            }
        }
        #endregion
    }
}
