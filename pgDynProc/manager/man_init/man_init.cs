﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;
using System.Data;
using pg_class.pg_commands;
using pg_class.pg_exceptions;
using System.Net.Sockets;
using pg_class.poolcn;

namespace pg_class
{
    public partial class manager
    {
        #region СТРУКТУРЫ ДЛЯ ВЗАИМОДЕЙСТВИЯ С БД

        private String sql_select_func_list(String SearchPath)
        {
            String schemas = "";
            String[] schemas_array = SearchPath.Split(",");

            foreach (String i in schemas_array)
            {
                schemas = schemas + String.Format("'{0}', ", i.Trim());
            }

            schemas = schemas.Substring(0, schemas.Length - 2);

            return String.Format("SELECT * FROM cfg_p_initproc_all($${0}$$)", schemas);
        }


        /// <summary>
        /// Лист команд взаимодействия с сервером PostgreSQL
        /// </summary>
        private List<NpgsqlCommandKey> command_list;

        #endregion

        #region ИНИЦИАЛИЗАЦИЯ КЛАССА ДОСТУПА К ДАННЫМ

        /// <summary>
        /// Метод инициализации класса
        /// </summary>
        protected void InitCommands()
        {
            //Вызов события журнала
            JournalEventArgs me = new JournalEventArgs(eEntity.manager, 0, "Инициализация менеджера данных", eAction.Init, eJournalMessageType.information);
            JournalMessageOnReceived(me);

            //Инициализация подключения к БД конфигурации
            connect cn = pool_.Connect_Get(true);

            //Вызов события журнала
            me = new JournalEventArgs(eEntity.manager, 0, "Инициализация команд менеджера данных", eAction.Init, eJournalMessageType.information);
            JournalMessageOnReceived(me);
            //Инициализация команд работы с БД
            InitCommand2(cn.CN);
            cn.UnLock();

            //Вызов события журнала
            String msg = String.Format("Инициализация команд менеджера данных завершена, загружено команд: {0}", command_list.Count);
            me = new JournalEventArgs(eEntity.manager, 0, msg, eAction.Init, eJournalMessageType.information);
            JournalMessageOnReceived(me);
        }

        #endregion

        #region ИНИЦИАЛИЗАЦИЯ КОМАНД КЛАССА ВЕРСИЯ 5
        /// <summary>
        /// Инициализация команд и таблиц данных менеджера доступа к БД
        /// </summary>
        private void InitCommand2(NpgsqlConnection CN_local)
        {
            NpgsqlCommand NCM;
            NpgsqlDataAdapter NDA;
            DataTable proc_DT;
            NpgsqlTransaction trans;

            //ИНИЦИАЛИЗАЦИЯ СВОЙСТВ ВЕРСИИ И СБОРКИ СЕРВЕРА БД
            trans = CN_local.BeginTransaction(IsolationLevel.RepeatableRead);
            NCM = new NpgsqlCommand();
            NCM.Connection = CN_local;
            NCM.Transaction = trans;
            NCM.CommandType = CommandType.Text;
            NCM.CommandText = "SELECT * FROM version();";
            try
            {
                //Запрос списка процедур API
                NCM.CommandText = sql_select_func_list(manager.Pg_ManagerSettings.SearchPath);                                    //"SELECT * FROM cfg_v_initproc_base;"
                proc_DT = new DataTable();
                NDA = new NpgsqlDataAdapter();
                NDA.SelectCommand = NCM;
                NDA.Fill(proc_DT);
                trans.Commit();

                if (proc_DT.Rows.Count > 0)
                {
                    UInt32 proc_oid;
                    String proc_name;
                    Int32 proc_args;
                    pg_argument[] proc_args_list;
                    String proc_argsignature;
                    Boolean proc_access;

                    NpgsqlCommand cmd;
                    NpgsqlCommandKey cmdk;

                    String arg_name;
                    String arg_type;
                    String arg_mode;

                    NpgsqlParameter cmd_sp;

                    //Инициализация листа команд БД
                    command_list = new List<NpgsqlCommandKey>();

                    //Создание списка процедур
                    foreach (DataRow dr in proc_DT.Rows)
                    {
                        proc_oid = Convert.ToUInt32(dr["oid"]);
                        proc_name = Convert.ToString(dr["proname"]);
                        proc_args = Convert.ToInt32(dr["proargs"]);
                        proc_args_list = (pg_argument[])dr["proargslist"];
                        proc_argsignature = Convert.ToString(dr["argsignature"]);
                        proc_access = Convert.ToBoolean(dr["access"]);
                        cmd = new NpgsqlCommand();
                        cmd.Connection = CN_local;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = proc_name;
                        if (proc_args > 0)
                        {
                            //Инициализация команды с параметрами
                            if (proc_args_list.Length > 0)
                            {
                                //Инициализация списка аргументов
                                foreach (pg_argument arg in proc_args_list)
                                {
                                    arg_name = arg.name;
                                    arg_type = arg.type;
                                    arg_mode = arg.mode;

                                    cmd_sp = cmd.Parameters.Add(arg_name, Name_To_NpgsqlType(arg_type));
                                    switch (arg_mode)
                                    {
                                        case "i":
                                            cmd_sp.Direction = ParameterDirection.Input;
                                            break;
                                        case "o":
                                            cmd_sp.Direction = ParameterDirection.Output;
                                            break;
                                        case "b":
                                            cmd_sp.Direction = ParameterDirection.InputOutput;
                                            break;
                                    }
                                }
                            }
                        }
                        cmdk = new NpgsqlCommandKey(cmd, proc_name, proc_args, proc_argsignature, proc_access);
                        //cmdk.Prepare();
                        command_list.Add(cmdk);
                    }
                }
            }
            catch (Exception ex)
            {
                trans.Rollback();
                StringBuilder sb = new StringBuilder();
                sb.Append("Сбой процедуры инициализации команд менеджера данных, дополнительные сведения: ");
                sb.Append(ex.Message);
                throw new PgManagerException(1101, sb.ToString(), ex.Message);
            }
        }
        #endregion
    }
}
