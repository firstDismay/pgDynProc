
DROP FUNCTION cfg_p_initproc_all(search_path text);
DROP VIEW IF EXISTS cfg_v_initproc_all;
DROP FUNCTION IF EXISTS cfg_procargs(procoid oid);
DROP TYPE IF EXISTS argument;


CREATE TYPE argument AS
(
	name character varying,
	type character varying,
	mode character varying
);

CREATE OR REPLACE FUNCTION cfg_procargs(
	procoid oid)
    RETURNS SETOF argument 
    LANGUAGE 'plpgsql'
    COST 100
    STABLE SECURITY DEFINER PARALLEL SAFE 
    ROWS 100

    SET search_path=bpd
AS $BODY$
DECLARE 
    lenarr INT DEFAULT 0;
    result public.argument;
    tmp RECORD;
    i INT DEFAULT 1;
 BEGIN

SELECT proargnames, proargtypes ,proallargtypes, proargmodes FROM pg_proc WHERE oid = "procoid" INTO tmp;

lenarr = array_length(tmp.proargnames, 1 );

IF (lenarr > 0 ) THEN

END IF;

WHILE i <= lenarr
    LOOP
        result."name" = tmp.proargnames[i];
        SELECT typname INTO result."type"  FROM pg_type WHERE oid = COALESCE(tmp.proallargtypes[i], tmp.proargtypes[i-1]);
        result."mode" = COALESCE(tmp.proargmodes[i], 'i');
        RETURN NEXT result;
        i = i + 1;
    END LOOP;
    END;
$BODY$;	
	
	

CREATE OR REPLACE VIEW cfg_v_initproc_all
 AS
 SELECT n.nspname::text, p.oid,
    p.proname,
    COALESCE(array_length(p.proargnames, 1), 0) AS proargs,
    ARRAY( SELECT cfg_procargs(p.oid) AS cfg_procargs) AS proargslist,
    p.prorettype,
    ((('"'::text || p.proname::text) || '"('::text) || array_to_string(ARRAY( SELECT t.typname
           FROM pg_type t
             JOIN ( SELECT g.i
                   FROM ( SELECT generate_series(array_lower(p.proargtypes, 1), array_upper(p.proargtypes, 1)) AS generate_series) g(i)) sub ON p.proargtypes[sub.i] = t.oid
          ORDER BY sub.i), ', '::text)) || ')'::text AS argsignature,
    has_function_privilege(((( '"' || n.nspname::text || '"."'::text || p.proname::text) || '"('::text) || array_to_string(ARRAY( SELECT (ns.nspname::text || '.'::text) || t.typname::text
           FROM pg_type t
             JOIN ( SELECT g.i
                   FROM ( SELECT generate_series(array_lower(p.proargtypes, 1), array_upper(p.proargtypes, 1)) AS generate_series) g(i)) sub ON p.proargtypes[sub.i] = t.oid
             JOIN ( SELECT pg_namespace.oid,
                    pg_namespace.nspname
                   FROM pg_namespace) ns ON ns.oid = t.typnamespace
          ORDER BY sub.i), ', '::text)) || ')'::text, 'execute'::text) AS access,
    p.proacl
   FROM pg_proc p
     JOIN pg_namespace n ON p.pronamespace = n.oid;
	 --WHERE n.nspname = 'public'::name;



CREATE OR REPLACE FUNCTION cfg_p_initproc_all(search_path text)
    RETURNS SETOF cfg_v_initproc_all
    LANGUAGE 'plpgsql'
    COST 100
    STABLE SECURITY DEFINER PARALLEL SAFE 
    ROWS 1000
AS $BODY$
	DECLARE
		command text;
    	search_path_array text[];
	BEGIN
    	command = 'SELECT ARRAY[' || search_path || ']::text[];';

		execute command INTO search_path_array;
		
		RETURN QUERY SELECT * FROM cfg_v_initproc_all
			WHERE nspname = ANY(search_path_array);
	END;
$BODY$;
	 
  
  SELECT * FROM cfg_p_initproc_all($$'public', 'plg'$$);
  