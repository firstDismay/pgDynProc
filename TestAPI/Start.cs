﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using pg_class;
using pg_class.pg_commands;

namespace TestAPI
{
    public partial class Start : Form
    {
        manager NEW_pg_class;
        public Start()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            

            pg_class.pg_settings Settings = new pg_class.pg_settings(txtUser.Text, txtPassword.Text, txtServer.Text, txtBase.Text);
            NEW_pg_class = manager.Instance();
            NEW_pg_class.Pool_Create(Settings);

            NpgsqlCommandKey cmd = NEW_pg_class.CommandByKey("cfg_p_initproc_all");

            cmd.Parameters["search_path"].Value = "'plg', 'public'";

            DataTable dt = new DataTable();
            cmd.Fill(dt);
            dgvFuncList.DataSource = dt;
        }
    }
}
