# pgDynProc
Library for dynamically API for calling functions of the target schema of PostgreSQL database over npgsql
Dynamic list of NpgsqlCommand server functions created in the schemas specified in the SearchPath variable.
Asynchronous call allowed. Uses a small internal pool to maintain context for asynchronous calls.

# Introduction

The project for dynamically loading server functions allows you to solve the following tasks:
1. Reduces the amount of code on the client side. As there is no need to describe the functions manually;
2. Prepares a shared container of named, ready-to-run functions with named parameters. Provides the transfer of self-describing state in accordance with the ideology of REST;
3. Provides built-in activity logging without additional overhead based on .Net event models;
4. Preserves the context of each function when called asynchronously;
5. Manages connections to the base, ensures the optimal number of required connections.


# Project details
      C# .Net 5.0 over NPGSQL (pool npgsql not use)
#      Use:
      //Execute sql:
      pgDynProc/pgDynProc/SQL/pgDynProc.sql
     
     //Connect:
      pg_class.pg_settings Settings = new pg_class.pg_settings(UserName, UserPassword, Server, Base);
      NEW_pg_class = manager.Instance();
      NEW_pg_class.Pool_Create(Settings);
      
      //Selecting a function by name:
      NpgsqlCommandKey cmd = NEW_pg_class.CommandByKey("NameFuction");
      
      //setting named parameter values:
      cmd.Parameters["ParamName1"].Value = value1;
      cmd.Parameters["ParamName2"].Value = value2;
      
      //execute the returning tuple:
      DataTable dt = new DataTable(); 
      cmd.Fill(dt);
      
      //execute the returning scalar:
      Type Value = (Type)cmdk.ExecuteScalar()
      
# Shell function example
      public vclass class_add( Int64 iid_group, Int64 iid_parent, String iname, String idesc, Boolean ion,
            Boolean ion_extensible, Boolean ion_abstraction, Int32 iid_unit, Int32 iid_unit_conversion_rule, Int64 ibarcode_manufacturer)
        {
            Int64 id = 0;
            Int32 error;
            String desc_error;
            NpgsqlCommandKey cmdk;
            //**********
            cmdk = CommandByKey("class_add");

            if (cmdk != null)
            {
                if (!cmdk.Access)
                {
                    throw new AccessDataBaseException(404, String.Format(@"Access is denied function: {0}!", cmdk.CommandText));
                }
            }
            else
            {
                throw new AccessDataBaseException(405, String.Format(@"Function not found: {0}!", cmdk.CommandText));
            }
            //=======================

            cmdk.Parameters["iid_group"].Value = iid_group;
            cmdk.Parameters["iid_parent"].Value = iid_parent;
            cmdk.Parameters["iname"].Value = iname;
            cmdk.Parameters["idesc"].Value = idesc;
            cmdk.Parameters["ion"].Value = ion;
            cmdk.Parameters["ion_extensible"].Value = ion_extensible;
            cmdk.Parameters["ion_abstraction"].Value = ion_abstraction;
            cmdk.Parameters["iid_unit"].Value = iid_unit;
            cmdk.Parameters["iid_unit_conversion_rule"].Value = iid_unit_conversion_rule;
            cmdk.Parameters["ibarcode_manufacturer"].Value = ibarcode_manufacturer;
            
            cmdk.ExecuteNonQuery();

            error = Convert.ToInt32(cmdk.Parameters["outresult"].Value);
            desc_error = Convert.ToString(cmdk.Parameters["outdesc"].Value);
            //SetLastTimeUsing();
            //=======================
            switch (error)
            {
                case 0:
                    id = Convert.ToInt64(cmdk.Parameters["outid"].Value);
                    if (id > 0)
                    {
                        vclass = class_act_by_id(id);
                    }
                    break;
                default:
                    JournalEventArgs me = new JournalEventArgs(id, eEntity.vclass, error, desc_error, eAction.Insert, eJournalMessageType.error);
                    JournalMessageOnReceived(me);
                    throw new PgDataException(error, desc_error);
            }
            return vclass;
        }
        
  # Shell class example
    
    public class function
    {
        protected function()
        {
        }
        
        public function(System.Data.DataRow row) : this()
        {
            
            if (row.Table.TableName == "cfg_v_initproc_base_desc")
            {
                oid = Convert.ToUInt32(row["oid"]);
                name = Convert.ToString(row["proname"]);
                args = Convert.ToInt32(row["proargs"]);
                argsignature = Convert.ToString(row["argsignature"]); 
                access = Convert.ToBoolean(row["access"]);
                description = Convert.ToString(row["description"]);
            }
            else
            {
                throw new ArgumentOutOfRangeException(String.Format("Input table name '{0}' does not match constructor constraints!", row.Table.TableName));
            }
        }
		...
	}
